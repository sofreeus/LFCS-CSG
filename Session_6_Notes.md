# Scheduling:

* at
* cron

# Archiving:

* tar
* cpio

# Compression:

* gzip
* bzip2
* 7z,zip,arj