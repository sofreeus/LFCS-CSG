Session 2
---

1) Confirm study teams.

 - Each student has partners, has chosen to study alone, or gets a partner now.
 - noon Thursdays?

2) Create users. Delete users. Create groups. Explore registry: /etc/passwd, /etc/shadow, /etc/group. Binaries: useradd, groupadd, userdel [-r], groupdel, usermod, id, getent

3) Write script to create users and groups and populate groups. Write and use a matching delete script.


Values for story-based scenarios
---

```
Company: RackWizards
CEO: Jan
CTO: Kim
Staff: Alice, Bob, Charlie
Consultants: Dani, Eve, Fred
Admins: Kim, Alice, Bob, Eve
```
